import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, ScrollView, Image, Button, Alert } from 'react-native';


class App1 extends React.Component {

    state = {
        username: '',
        password: ''
    };


    render() {
        return (
            
            <View style={styles.container} >
                <View style={styles.content}>
                    <View style={[styles.layout1, styles.center]}>
                        <View style={[styles.logo, styles.center]}>
                            <Image source={require('./profile2.jpg')} style={[styles.logo]} />
                        </View>
                    </View>

                    <Text> {this.state.username}-{this.state.password} </Text>
                    <View style={[styles.layout2, styles.center]}>
                        <View style={[styles.textInput1, styles.center, styles.radius]}>
                            <TextInput
                                style={styles.style}
                                onChangeText={(username) => this.setState({ username })}>
                                TextInput
                            </TextInput>
                        </View>

                        <View style={[styles.textInput2, styles.center, styles.radius]}>
                            <TextInput
                                style={styles.style}
                                onChangeText={(password) => this.setState({ password })}>
                                TextInput
                            </TextInput>
                        </View>

                        <View style={[styles.touchable, styles.radius, styles.center]}>
                            {/* <Button title="TouchableOpacity" onPress={() => { Alert.alert('ฝุ่นเยอะจัง', 'เฮ้อออ....') }} /> */}
                            <Button
                                
                                onPress={() => { Alert.alert('Welcome', 'Yeah....') }}
                                title="TouchableOpacity"
                                color="#263A8A"
                                
                            />
                            {/* <Text style={styles.style}></Text> */}
                        </View>

                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#212F3C',
        flex: 1
    },

    content: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    layout1: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    layout2: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 100,
        width: 200,
        height: 200
    },

    image: {
        backgroundColor: '#7DCEA0',
        margin: 50,
    },

    textInput1: {
        backgroundColor: '#505392',
        flex: 1,
        padding: 12,
        margin: 2
    },

    textInput2: {
        backgroundColor: '#505392',
        flex: 1,
        padding: 12,
        margin: 2
    },

    touchable: {
        backgroundColor: '#263A8A',
        flex: 1,
        margin: 70,
    },
    style: {
        color: 'white',
        textAlign: 'center',
        fontSize: 15,
    },
    radius: {
        borderRadius: 100,
        width: 400,
        height: 400
    }


})
export default App1
