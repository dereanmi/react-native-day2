import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Modal, TouchableOpacity } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class App2 extends React.Component {

    state = {
        isShowModal: false,
        message: ''
    };

    onShowModal() {
        this.setState({ isShowModal: true });
    }

    onHideModal() {
        this.setState({ isShowModal: false });
    }

    render() {
        return (

            <View style={styles.container} >
                <Modal
                    visible={this.state.isShowModal}
                    transparent={true}
                >
                    <TouchableOpacity
                        onPress={() => { this.onHideModal() }}
                        style={[styles.modalLayout,styles.center]}
                    >
                        <Text style={styles.textStyle}>FOODS</Text>

                    </TouchableOpacity>
                </Modal>

                <View style={styles.header}>
                    <Text style={styles.headerText}>Foods</Text>
                </View>

                <View style={styles.content}>


                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./food1.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./food2.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./food3.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./food4.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./food5.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./food6.jpeg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>



                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#82E0AA',
        alignItems: 'center'
    },

    headerText: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 30
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    box1: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },

    box2: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },

    row: {
        backgroundColor: '#85C1E9',
        flex: 1,
        margin: 14,
        flexDirection: 'row'
    },
    logo: {
        borderRadius: 30,
        width: 150,
        height: 150
    },
    modalLayout: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    },

    textStyle: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    }


})
export default App2
