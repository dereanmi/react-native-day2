/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableHighlight, TextInput } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      <TextInput style = {{height: 40, borderColor: 'gray', borderWidth: 1}}/>
        <Button title="สวัสดีวันอังคาร" onPress={() => { Alert.alert('ฝุ่นเยอะจัง', 'เฮ้อออ....') }} />
        <TouchableHighlight onPress={() => { Alert.alert('yahhh','welcome') }}>
          <Text style={styles.welcome}>Welcome to React Native!</Text>
        </TouchableHighlight>

        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default App
